import { GeneratorOptions } from "./generatorOptions.type";
import { range } from "@b08/array";
import { memoize } from "@b08/memoize";

export function updateString(src: string, indent: number, options: GeneratorOptions): string {
  const replaced = getIndent(indent) + src
    .replace(/'|"/g, options.quotes)
    .replace(/\r?\n|\r/g, options.linefeed + getIndent(indent));

  return src.endsWith("\n") || src.endsWith("\r")
    ? replaced.slice(0, replaced.length - indent)
    : replaced;
}

const getIndent = memoize(function getIndent(indent: number): string {
  return range(indent).map(() => " ").join("");
});
