import { GeneratorOptions, LineFeed, Quotes } from "./generatorOptions.type";

interface InternalOptions {
  linefeed: LineFeed;
  apostropheChar: Quotes;
  quoteChar: Quotes;
}

export class Generator<T extends GeneratorOptions = GeneratorOptions> {
  public indent: string = "";
  private opts: InternalOptions;
  private lines: string[] = [];
  private eolPrinted: boolean = true;

  constructor(public options?: T) {
    this.opts = {
      linefeed: options?.linefeed || "\n",
      quoteChar: options?.quotes || "\"",
      apostropheChar: options?.quotes || "'"
    };
  }

  public setIndent(indent: string): Generator {
    this.indent = indent;
    return this;
  }

  public withIndent(indent: string, func: (g: Generator) => Generator): Generator {
    let oldIndent = this.indent;
    this.indent += indent;
    func(this);
    this.indent = oldIndent;
    return this;
  }

  public append(text: string): Generator {
    if (this.eolPrinted) { this.lines.push(this.indent); }
    this.lines.push(text);
    this.eolPrinted = false;
    return this;
  }

  public eol(): Generator {
    this.eolPrinted = true;
    this.lines.push(this.opts.linefeed);
    return this;
  }

  public quote(): Generator {
    return this.append(this.opts.quoteChar);
  }

  public apostrophe(): Generator {
    return this.append(this.opts.apostropheChar);
  }

  public toString(): string {
    return this.lines.join("");
  }
}
