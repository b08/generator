import { isNumber } from "@b08/type-checks";
import { GeneratorOptions } from "./generatorOptions.type";
import { updateString } from "./updateString";

const defaultOptions: GeneratorOptions = {
  quotes: "\"",
  linefeed: "\n"
};

export function generator<TP extends any[], TP2 extends [...TP, GeneratorOptions, number?]>(func: (...p: TP) => string): (...p: TP2) => string {
  return function (...p: TP2): string {
    let parms: TP;
    let indent = p[p.length - 1];
    let options: GeneratorOptions;
    if (isNumber(indent)) {
      options = p[p.length - 2] as GeneratorOptions;
      parms = p.slice(0, p.length - 2) as TP;
    } else {
      options = indent;
      indent = 0;
      parms = p.slice(0, p.length - 1) as TP;
    }
    options = { ...defaultOptions, ...options };
    const generated = func.call(options, ...parms);
    return updateString(generated, indent, options);
  };
}

export function lines<T>(arr: T[], func: (t: T) => string): string {
  return arr.map(func).join("\n");
}
