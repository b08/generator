import { test } from "@b08/test-runner";
import { generator, updateString } from "../src";

test("updateString does not touch string without quotes, linefeeds and indent", t => {
  // arrange
  const src = "123";
  // act
  const result = updateString(src, 0, {});

  // assert
  t.equal(result, src);
});

test("updateString indents a string", t => {
  // arrange
  const src = "123";
  // act
  const result = updateString(src, 3, {});

  // assert
  t.equal(result, "   123");
});

test("updateString indents a multiline string", t => {
  // arrange
  const src = `123
  456
  789
`;
  // act
  const result = updateString(src, 2, { linefeed: "\n" });

  // assert
  const expected = `  123
    456
    789
`;
  t.equal(result, expected);
});

test("updateString replaces linux linefeed", t => {
  // arrange
  const src = `123\n456`;
  // act
  const result = updateString(src, 1, { linefeed: "\r\n" });

  // assert
  const expected = ` 123\r\n 456`;
  t.equal(result, expected);
});

test("updateString replaces windows linefeed", t => {
  // arrange
  const src = `123\r\n456`;
  // act
  const result = updateString(src, 1, { linefeed: "\n" });

  // assert
  const expected = ` 123\n 456`;
  t.equal(result, expected);
});

test("updateString replaces mac linefeed", t => {
  // arrange
  const src = `123\r456`;
  // act
  const result = updateString(src, 1, { linefeed: "\n" });

  // assert
  const expected = ` 123\n 456`;
  t.equal(result, expected);
});

test("updateString replaces quotes", t => {
  // arrange
  const src = `123"456`;
  // act
  const result = updateString(src, 0, { quotes: "'" });

  // assert
  const expected = `123'456`;
  t.equal(result, expected);
});

test("updateString replaces apostrophe", t => {
  // arrange
  const src = `123'456`;
  // act
  const result = updateString(src, 0, { quotes: `"` });

  // assert
  const expected = `123"456`;
  t.equal(result, expected);
});

test("updateString works in generator", t => {
  // arrange

  // act
  const g = generator((a: number) => a.toString());
  const result = g(10, {}, 1);

  // assert
  const expected = ` 10`;
  t.equal(result, expected);
});

test("updateString in generator should have optional indent", t => {
  // arrange

  // act
  const g = generator((a: number) => a.toString());
  const result = g(10, {});

  // assert
  const expected = `10`;
  t.equal(result, expected);
});
