import { test } from "@b08/test-runner";
import { Generator } from "../src";

test("generator should output a string", expect => {
  // arrange
  const target = new Generator();

  // act
  const result = target.append("123").toString();

  // assert
  expect.equal(result, "123");
});

test("generator should output a string with linefeed", expect => {
  // arrange
  const target = new Generator();

  // act
  const result = target.append("123").eol().toString();

  // assert
  expect.equal(result, "123\n");
});

test("generator should insert indent after eol ", expect => {
  // arrange
  const target = new Generator();

  // act
  const result = target.setIndent(" ")
    .append("123").append("234").eol()
    .append("345").append("456").toString();

  // assert
  expect.equal(result, " 123234\n 345456");
});

test("generator should replace quote and apostrophes with value from options", expect => {
  // arrange
  const target = new Generator({ quotes: "'" });

  // act
  const result = target.quote().append("123").apostrophe().toString();

  // assert
  expect.equal(result, "'123'");
});

test("generator should work with indent ", expect => {
  // arrange
  const target = new Generator();
  const expected = `123
  234
    345
456`;
  // act
  const result = target.append("123").eol()
    .withIndent("  ", g =>
      g.append("234").eol()
        .withIndent("  ", g => g.append("345").eol())
    )
    .append("456").toString();

  // assert
  expect.equal(result, expected);
});
